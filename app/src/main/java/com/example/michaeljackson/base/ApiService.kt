package com.example.michaeljackson.base

import com.example.michaeljackson.api.ituneApi
import com.example.michaeljackson.songs.model.*

//import com.example.michaeljackson.util.ConstantKey
import io.reactivex.Observable

class ApiService private constructor() : BaseService() {
    private lateinit var api: ituneApi

    companion object {
        private val hospitalService = ApiService()
        fun getInstance(): ApiService {
            if (!hospitalService::api.isInitialized) {
                hospitalService.api = baseServiceGenerator.generate(ituneApi::class.java)
            }
            return hospitalService;
        }
    }
    fun getSongList(request: SongModel): Observable<SongModel> {
        return api.getSongList(request)
    }

}
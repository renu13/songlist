package com.example.michaeljackson.base

import com.google.gson.GsonBuilder
//import com.hosptal.groinfo.BuildConfig
//import com.hosptal.groinfo.api.HospitalApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class BaseServiceGenerator  {
    private var baseUrl = "https://itunes.apple.com/"
    private var httpClient = OkHttpClient.Builder()

    private var gson = GsonBuilder()
        .enableComplexMapKeySerialization()
        .serializeNulls()
        //.setDateFormat(BuildConfig.DATE_TIME_FORMAT)
        .setPrettyPrinting()
        .setLenient()
        .create()

    private var builder = Retrofit
        .Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())

    private var retrofit = builder.build()


    companion object {
        private val baseServiceGenerator = BaseServiceGenerator()
        fun getInstance(): BaseServiceGenerator {

            /*val authInterceptor = BaseAuthInterceptor()
            if (!baseServiceGenerator.httpClient.networkInterceptors().contains(authInterceptor)) {
                baseServiceGenerator.httpClient.networkInterceptors().add(authInterceptor)
            }*/

            /*if (BuildConfig.DEBUG) {
                val loggerInterceptor = HttpLoggingInterceptor()
                if (!baseServiceGenerator.httpClient.networkInterceptors().contains(loggerInterceptor)) {
                    loggerInterceptor.level = HttpLoggingInterceptor.Level.BODY
                    baseServiceGenerator.httpClient.networkInterceptors().add(loggerInterceptor)
                }
            }*/

            val loggerInterceptor = HttpLoggingInterceptor()

            if (!baseServiceGenerator.httpClient.networkInterceptors().contains(loggerInterceptor)) {
                loggerInterceptor.level = HttpLoggingInterceptor.Level.BODY
                baseServiceGenerator.httpClient.networkInterceptors().add(loggerInterceptor)
            }
            baseServiceGenerator.builder.client(
                baseServiceGenerator.httpClient
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .build()
            )

            baseServiceGenerator.retrofit = baseServiceGenerator.builder.build()
            return baseServiceGenerator
        }
    }
    fun <S> generate(serviceClass: Class<S>): S {
        return retrofit.create(serviceClass)
    }
}
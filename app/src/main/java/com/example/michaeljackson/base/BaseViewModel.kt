package com.example.michaeljackson.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import android.view.View

open class BaseViewModel:ViewModel(),Observable {

    val showProgress: MutableLiveData<Boolean> by lazy { MutableLiveData<Boolean>() }
    val snakBarMsg: MutableLiveData<String> by lazy { MutableLiveData<String>() }

    private val callbacks: PropertyChangeRegistry = PropertyChangeRegistry()
    val buttonSaveClicked: MutableLiveData<Boolean> by lazy { MutableLiveData<Boolean>() }

    fun isShowProgress(): LiveData<Boolean> {
        return showProgress
    }

    fun getSnackBarMsg(): LiveData<String> {
        return snakBarMsg
    }

    /**
     * Notifies observers that all properties of this instance have changed.
     */
    fun notifyChange() {
        callbacks.notifyCallbacks(this, 0, null)
    }

    /**
     * Notifies observers that a specific property has changed. The getter for the
     * property that changes should be marked with the @Bindable annotation to
     * generate a field in the BR class to be used as the fieldId parameter.
     *
     * @param fieldId The generated BR id for the Bindable field.
     */
    fun notifyPropertyChanged(fieldId: Int) {
        callbacks.notifyCallbacks(this, fieldId, null)
    }

    fun doSaveClicked(view: View) {
        buttonSaveClicked.value = true
    }


    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        callbacks.remove(callback)
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        callbacks.add(callback)
    }


}
package com.example.michaeljackson.api

import com.example.michaeljackson.songs.model.*


import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface ituneApi {


    @Headers("Content-Type: application/json")
    @POST("/search?term=Michael+jackson")
    fun getSongList(@Body request: SongModel): Observable<SongModel>


}

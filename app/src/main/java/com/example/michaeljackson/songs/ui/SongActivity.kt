package com.example.michaeljackson.songs.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import  com.example.michaeljackson.R

class SongActivity : AppCompatActivity() {
    lateinit var songViewHolder: SongViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_song)
        songViewHolder = SongViewModel(this, this)
        songViewHolder.displaySongs(this,findViewById(R.id.rcvSongs))
    }

}
package com.example.michaeljackson.songs.model

class SongList {
    var wrapperType:String="";
    var trackName:String="";
    var artistName:String=""
    var collectionCensoredName:String=""
    var trackCensoredName:String=""
    var artworkUrl100:String="";
    var collectionName:String="";
}
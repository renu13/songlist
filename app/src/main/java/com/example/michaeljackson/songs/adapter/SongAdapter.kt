package com.example.michaeljackson.songs.adapter



import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.michaeljackson.songs.model.SongList
//import com.bumptech.glide.Glide
//import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.michaeljackson.databinding.SongListBinding
import com.example.michaeljackson.songs.ui.DetailsActivity

//import com.hosptal.groinfo.cutomer.model.HospitalListModel
//import com.hosptal.groinfo.databinding.HospitallistadapterBinding
//import com.hosptal.groinfo.cutomer.ui.HospitalProfileActivity


class SongAdapter(var mContext: Context, var listJackson: Array<SongList>) :
    RecyclerView.Adapter<SongAdapter.JacksonViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JacksonViewHolder {
        val layoutInflater = LayoutInflater.from(mContext)
        var JacksonListAdapter = SongListBinding.inflate(layoutInflater, parent, false)
        return JacksonViewHolder(JacksonListAdapter)
    }

    override fun getItemCount(): Int {
        return listJackson?.size
    }

    override fun onBindViewHolder(holder: JacksonViewHolder, position: Int) {
        var listModel = listJackson[position]

        holder.bindModel(listModel)
    }

    inner class JacksonViewHolder(var view: SongListBinding) :
        RecyclerView.ViewHolder(view!!.root) {

        fun bindModel(list: SongList) {

            view.viewModel = list
view.songName.text=list.trackName
            view.songDesc.text=list.collectionName
            Glide.with(mContext).load(list.artworkUrl100)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(view.img1)
            view.lylist.setOnClickListener{
                var intent = Intent(mContext, DetailsActivity::class.java)
                intent.putExtra("cName", list.artworkUrl100)
                intent.putExtra("tName", list.trackName)
                intent.putExtra("wType", list.wrapperType)
                intent.putExtra("aName", list.artistName)
                intent.putExtra("ccName", list.collectionCensoredName)
                intent.putExtra("trName", list.trackCensoredName)
                mContext.startActivity(intent)
            }

//            if(list.currentStatus.equals("yes")){
////                view.rowTopratedJackson.background.setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY )
////                view.rowTopratedJackson.setBackgroundColor(Color.parseColor("#f0f0f0"))
//                view.state.setText("Available")
//                view.state.setTextColor(Color.parseColor("#4CAF50"))}
//            else if(list.currentStatus.equals("no")){
//                view.state.setText("Closed")
//                view.state.setTextColor(Color.parseColor("#FF0000"))
//            }
//            if(list.verify.equals("1")){
//                view.checkgreen.visibility= View.VISIBLE}
//            else{
//                view.checkgreen.visibility= View.GONE
//            }
//            view.layoutHospList.setOnClickListener {
//                var intent = Intent(mContext, JacksonProfileActivity::class.java)
//                intent.putExtra("hospitalId", list.reg_id)
//                mContext.startActivity(intent)
//            }
//            Glide.with(mContext).load(list.hosp_img)
//                .diskCacheStrategy(DiskCacheStrategy.NONE)
//                .skipMemoryCache(true)
//                .into(view.imageView)
//
//        }
        }
    }
}
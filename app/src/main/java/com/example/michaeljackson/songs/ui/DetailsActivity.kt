package com.example.michaeljackson.songs.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.michaeljackson.R

class DetailsActivity : AppCompatActivity() {
    private lateinit var wrapperType: String
    private lateinit var trackName: String
    private lateinit var artistName: String
    private lateinit var collectionCensoredName: String
    private lateinit var trackCensoredName: String
    private lateinit var picture: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        wrapperType = intent.getStringExtra("wType").toString()
        trackName = intent.getStringExtra("tName").toString()
        artistName = intent.getStringExtra("aName").toString()
        collectionCensoredName = intent.getStringExtra("ccName").toString()
        trackCensoredName = intent.getStringExtra("trName").toString()
        picture = intent.getStringExtra("cName").toString()
        findViewById<TextView>(R.id.wType).setText(wrapperType)
        findViewById<TextView>(R.id.tName).setText(trackName)
        findViewById<TextView>(R.id.aName).setText(artistName)

        findViewById<TextView>(R.id.tcName).setText(trackCensoredName)

        Glide.with(this).load(picture)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)
            .into(findViewById<ImageView>(R.id.imgart))
    }
}
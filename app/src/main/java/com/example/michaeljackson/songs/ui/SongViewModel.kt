package com.example.michaeljackson.songs.ui
import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


import com.example.michaeljackson.base.BaseViewModel
import com.example.michaeljackson.base.ApiService

import com.example.michaeljackson.songs.adapter.*
import com.example.michaeljackson.songs.model.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


// Sample Code
class SongViewModel(var mContext: Context, var activity: SongActivity) : BaseViewModel() {

    private var Service: ApiService = ApiService.getInstance()
    var SongListModel = SongModel()
    val SongResponseModel: MutableLiveData<SongModel> by lazy { MutableLiveData<SongModel>() }

    @SuppressLint("CheckResult")
    fun displaySongs(context: Context,
                      recyclerView: RecyclerView){
        Service.getSongList(SongListModel)
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ headerList->


                SongResponseModel.value = headerList
                var songAdapter=SongAdapter(context,SongResponseModel.value?.results!!)
                recyclerView.layoutManager= LinearLayoutManager(context)
                recyclerView.adapter=songAdapter


            }, { error ->
//                showProgress.value = false
//                snakBarMsg.value = SongResponseModel.value?.message
            }, {
//                showProgress.value = false
            })
    }

}